package main

const (
	// HideBlocked when true, ignores all blocked users
	HideBlocked = true

	// BackgroundColor self explanatory
	// Acceptable values: tcell.Color*, -1, 0-255 (terminal colors)
	BackgroundColor = 0xFFFFFF

	// EmbedColLimit controls the line
	EmbedColLimit = 75
)
